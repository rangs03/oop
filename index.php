<?php

require_once("Animal.php");
require_once("Frog.php");
require_once("Ape.php");

$sheep  = new animal("shaun");
echo "Name : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>"; // 4
echo "cold blooded : $sheep->cold_blooded <br>"; // "no"

$kodok = new Frog("buduk");
echo "<br>Name : $kodok->name <br>"; // "buduk"
echo "Legs : $kodok->legs <br>"; // 4
echo "cold blooded: $kodok->cold_blooded <br>"; // "no"
echo "Jump : ".$kodok->jump() ; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "<br><br>Name  : $sungokong->name <br>"; // "sungokong"
echo "Legs : $sungokong->legs <br>"; // 2
echo "cold blooded : $sungokong->cold_blooded <br>"; // "no"
echo "Yell : ". $sungokong->yell(); // "Auooo"


?>